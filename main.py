import cards
import players
import table
import user_interface


def generate_deck(seed=None):
    """
    Generate a poker deck of cards and shuffle it
    :param seed: optional parameter, set a seed for the random shuffle
    :return: the shuffled deck
    """
    new_deck = cards.PokerDeck()
    new_deck.shuffle(seed)
    print(f"-- The seed is {new_deck.seed} --")
    return new_deck


def create_player(player_name: str):
    """
    Create the main player with a specified name
    :param player_name: name of the player to create
    :return: the created player
    """
    return players.Player(player_name)


def create_ais(nb_ai: int):
    """
    Create AI players
    :param nb_ai: the number of AI to create
    :return: a list of the created AIs
    """
    list_ais = []
    for i in range(nb_ai):
        list_ais.append(players.Player(f"Player{i + 1}"))
    return list_ais


def distribute_cards_players(card_deck: cards.Deck, nb_cards: int, game_players: list):
    """
    Distribute cards to the players
    Each in turn, each player has a card
    :param card_deck: the deck to distribute
    :param nb_cards: the number of cards to give to each player
    :param game_players: the list of players
    """
    distribution = card_deck.distribute(nb_cards, len(game_players))
    for index, distributed_cards in enumerate(distribution):
        game_players[index].cards.append(distributed_cards)


def distribute_cards_table(card_deck: cards.Deck, nb_cards: int, game_table: table.Table):
    """
    Lay cards on the table
    :param card_deck: the deck to distribute
    :param nb_cards: the number of cards to lay
    :param game_table: the table on which the cards are laid
    """
    distributed_cards = card_deck.distribute(nb_cards, 1)
    game_table.cards.append(distributed_cards[0])


def get_winner(game_players: list, game_table: table.Table):
    """
    Compare players hand and get the winner
    :param game_players: the list of players
    :param game_table: the table on which the cards are laid
    :return: the winner
    """
    hands = []
    for player in game_players:
        hand_cards = player.cards[0] + game_table.cards[0]
        hand = cards.Hand(hand_cards)
        hands.append((player, hand))
        print(f"{player.name} hand is a {hand}.")
    hands.sort(reverse=True, key=lambda x: x[1])
    print(f"{hands[0][0].name} wins with a {hands[0][1]}")
    return hands[0][0]


if __name__ == '__main__':
    # -- init deck and table
    deck = generate_deck()
    table = table.Table()
    # -- init main player and AIs
    ai1, ai2, ai3 = create_ais(3)
    main_player = create_player("Main player")
    list_players = [ai1, ai2, ai3, main_player]

    # -- card distribution
    # give 2 cards to each player
    distribute_cards_players(deck, 2, list_players)
    # put 3 cards on the table
    distribute_cards_table(deck, 3, table)

    # -- Eval hands
    winner = get_winner(list_players, table)

    user_interface.userInterface(list_players, table)

