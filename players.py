import cards


class Player:
    def __init__(self, name: str):
        self.__name = name
        self.__cards = cards.Deck()
        self.__high_score = 0
        print(f"-- New player created: {name} --")

    @property
    def name(self):
        return self.__name

    @property
    def cards(self):
        return self.__cards

    @property
    def high_score(self):
        return self.__high_score

    def __str__(self):
        return f"{self.name}"

    def __repr__(self):
        return f"{self.name} has {len(self.cards)} cards and a high score of {self.high_score}"
