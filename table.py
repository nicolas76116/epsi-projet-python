import cards


class Table:
    def __init__(self):
        self.__cards = cards.Deck()
        print("-- Game table created --")

    @property
    def cards(self):
        return self.__cards

    def __repr__(self):
        return f"There {'are' if len(self.cards[0]) > 1 else 'is'} {len(self.cards[0])} " \
               f"card{'s' if len(self.cards[0]) > 1 else ''} on the table: {self.cards}"
