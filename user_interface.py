import players
import tkinter as tk
from tkinter import *

# Variables
"""cards"""
temp_cards = ""
player1_card1_img = ""
player1_card2_img = ""
player2_card1_img = ""
player2_card2_img = ""
player3_card1_img = ""
player3_card2_img = ""
player4_card1_img = ""
player4_card2_img = ""

"""colors"""
bg_color = "green"
btm_color = "brown"
border_board = "yellow"
header_color = "light green"

"""texts"""
title_app_window = "EPSI - Poker Project"
title_app = "Poker Project"
play_text = "PLAY"
quit_text = "QUIT"
input_label = "your username"
board_label = "Board"

"""sizes"""
width_window = 480
height_window = 720
width_canvas = 300
height_canvas = 300
width_cards = 100
height_cards = 200
width_main_card = 180

"""fonts"""
ubuntu_font15 = "Ubuntu 15"
ubuntu_font20 = "Ubuntu 20"
ubuntu_font30 = "Ubuntu 30"

"""paths"""
logo_image_path = "assets/images/poker"
card_image_path = "assets/images/cards/"


# Main function
def userInterface(list_players: any, table: any):
    """
    Retrieve cards to display them on the interface
    :param list_players: the list of players
    :param table: the table on which the cards are laid
    """
    # Display view
    def show_frame(frame):
        """
        Retrieve cards to display them on the interface
        :param frame: display the current frame
        """
        frame.tkraise()

    def enableBtn():
        if username_variable.get() != "":
            next_frame()

    # Display next view
    def next_frame():
        show_frame(game_frame)

    # Distribution of players
    for one_player in list_players:
        for card in one_player.cards:
            temp_cards = card

            if one_player == list_players[0]:
                global player1_card1_img
                global player1_card2_img
                player1_card1_img = temp_cards[0]
                player1_card2_img = temp_cards[1]
            elif one_player == list_players[1]:
                global player2_card1_img
                global player2_card2_img
                player2_card1_img = temp_cards[0]
                player2_card2_img = temp_cards[1]
            elif one_player == list_players[2]:
                global player3_card1_img
                global player3_card2_img
                player3_card1_img = temp_cards[0]
                player3_card2_img = temp_cards[1]
            else:
                global player4_card1_img
                global player4_card2_img
                player4_card1_img = temp_cards[0]
                player4_card2_img = temp_cards[1]

    t1, t2, t3 = str(table).split(",")
    sub_t1 = t1.split(" ", 7)
    sub_t2 = t2.split(" ", 1)
    sub_t3 = t3.split(" ", 1)

    # Create window
    window = tk.Tk()
    window.title(title_app_window)
    window.geometry(f"{height_window}x{width_window}")
    window.minsize(height_window, width_window)
    window.maxsize(height_window, width_window)
    window.rowconfigure(0, weight=1)
    window.columnconfigure(0, weight=1)
    window.iconbitmap(f"{logo_image_path}_icon.ico")
    window.config(background=bg_color)

    start_frame = Frame(window, bg=bg_color)
    game_frame = Frame(window, bg=bg_color)

    for frame in (start_frame, game_frame):
        frame.grid(row=0, column=0, sticky='nsew')

    # VIEW 1
    image = tk.PhotoImage(file=f"{logo_image_path}.png").zoom(35).subsample(32)
    canvas = Canvas(start_frame, width=width_canvas, height=height_canvas, bg=bg_color, bd=0, highlightthickness=0)
    canvas.create_image(width_canvas / 2, height_canvas / 2, image=image)
    canvas.grid(row=0, column=0, sticky=W)
    canvas.pack()

    right_frame = Frame(start_frame, bg=bg_color)

    label_title = Label(right_frame, text=title_app, font=ubuntu_font30, bg=bg_color, fg="white", pady=0)
    label_title.pack()

    label_input_player = Label(right_frame, text=input_label, font=ubuntu_font15, bg=bg_color, fg="white")
    label_input_player.pack()

    username_variable = StringVar()
    input_player = Entry(right_frame, text=username_variable, font=ubuntu_font15, bg="white", fg="black")
    input_player.pack(fill=X)

    start_btn = Button(right_frame, text=play_text, font=ubuntu_font15, command=lambda: enableBtn())

    start_btn.pack(side=BOTTOM, fill=X, pady=20)
    right_frame.pack()

    # VIEW 2
    top_frame = tk.PanedWindow(game_frame, bg=bg_color, bd=5, height=200)
    middle_frame = tk.PanedWindow(game_frame, bg=bg_color, bd=5, height=160)
    bottom_frame = tk.PanedWindow(game_frame, bg=btm_color)

    quit_btn = Button(bottom_frame, text=quit_text, font=ubuntu_font15, command=window.quit)
    quit_btn.pack(side=BOTTOM)

    top_frame.pack(fill="both")
    middle_frame.pack(fill="both", expand=True)
    bottom_frame.pack(side="bottom", fill="x")

    left_player = tk.PanedWindow(top_frame, bg=bg_color, orient=VERTICAL, width=240)
    mid_player = tk.PanedWindow(top_frame, bg=bg_color, orient=VERTICAL, width=240)
    right_player = tk.PanedWindow(top_frame, bg=bg_color, orient=VERTICAL, width=240)
    bottom_player = tk.PanedWindow(middle_frame, bg=bg_color, orient=VERTICAL, width=360)
    bottom_board = tk.PanedWindow(middle_frame, bg=border_board, bd=5, orient=VERTICAL, width=360)

    """headers"""
    p1_title_frame = tk.Frame(left_player)
    player1_title = tk.Label(p1_title_frame, text=list_players[0], font=ubuntu_font20, bg=header_color, width=16)
    player1_title.pack()
    p2_title_frame = tk.Frame(mid_player)
    player2_title = tk.Label(p2_title_frame, text=list_players[1], font=ubuntu_font20, bg=header_color, width=16)
    player2_title.pack()
    p3_title_frame = tk.Frame(right_player)
    player3_title = tk.Label(p3_title_frame, text=list_players[2], font=ubuntu_font20, bg=header_color, width=16)
    player3_title.pack()
    p4_title_frame = tk.Frame(bottom_player)
    player4_title = tk.Label(p4_title_frame, textvariable=username_variable, font=ubuntu_font20, bg=header_color,
                             width=16)
    player4_title.pack(fill="x")
    board_title_frame = tk.Frame(bottom_board)
    board_title = tk.Label(board_title_frame, text=board_label, font=ubuntu_font20, bg=header_color, width=16)
    board_title.pack(fill="x")

    """body"""
    # PLAYER 1
    p1_body = tk.PanedWindow(left_player, orient=HORIZONTAL, bg=bg_color)

    player1_card1 = tk.Frame(p1_body, bg=bg_color, padx=8)
    p1_c1_img = tk.PhotoImage(file=f"{card_image_path}{player1_card1_img}.png").subsample(9)
    p1_card1 = Canvas(player1_card1, width=width_cards, height=height_cards, bg=bg_color, bd=0, highlightthickness=0)
    p1_card1.create_image(width_cards / 2, width_cards / 1.4, image=p1_c1_img)
    p1_card1.pack()

    player1_card2 = tk.Frame(p1_body, bg=bg_color)
    p1_c2_img = tk.PhotoImage(file=f"{card_image_path}{player1_card2_img}.png").subsample(9)
    p1_card2 = Canvas(player1_card2, width=width_cards, height=height_cards, bg=bg_color, bd=0, highlightthickness=0)
    p1_card2.create_image(width_cards / 2, width_cards / 1.4, image=p1_c2_img)
    p1_card2.pack()

    p1_body.add(player1_card1)
    p1_body.add(player1_card2)

    # PLAYER 2
    p2_body = tk.PanedWindow(mid_player, orient=HORIZONTAL, bg=bg_color)

    player2_card1 = tk.Frame(p2_body, bg=bg_color, padx=8)
    p2_c1_img = tk.PhotoImage(file=f"{card_image_path}{player2_card1_img}.png").subsample(9)
    p2_card1 = Canvas(player2_card1, width=width_cards, height=height_cards, bg=bg_color, bd=0, highlightthickness=0)
    p2_card1.create_image(width_cards / 2, width_cards / 1.4, image=p2_c1_img)
    p2_card1.pack()

    player2_card2 = tk.Frame(p2_body, bg=bg_color)
    p2_c2_img = tk.PhotoImage(file=f"{card_image_path}{player2_card2_img}.png").subsample(9)
    p2_card2 = Canvas(player2_card2, width=width_cards, height=height_cards, bg=bg_color, bd=0, highlightthickness=0)
    p2_card2.create_image(width_cards / 2, width_cards / 1.4, image=p2_c2_img)
    p2_card2.pack()

    p2_body.add(player2_card1)
    p2_body.add(player2_card2)

    # PLAYER 3
    p3_body = tk.PanedWindow(right_player, orient=HORIZONTAL, bg=bg_color)

    player3_card1 = tk.Frame(p3_body, bg=bg_color, padx=8)
    p3_c1_img = tk.PhotoImage(file=f"{card_image_path}{player3_card1_img}.png").subsample(9)
    p3_card1 = Canvas(player3_card1, width=width_cards, height=height_cards, bg=bg_color, bd=0, highlightthickness=0)
    p3_card1.create_image(width_cards / 2, width_cards / 1.4, image=p3_c1_img)
    p3_card1.pack()

    player3_card2 = tk.Frame(p3_body, bg=bg_color)
    p3_c2_img = tk.PhotoImage(file=f"{card_image_path}{player3_card2_img}.png").subsample(9)
    p3_card2 = Canvas(player3_card2, width=width_cards, height=height_cards, bg=bg_color, bd=0, highlightthickness=0)
    p3_card2.create_image(width_cards / 2, width_cards / 1.4, image=p3_c2_img)
    p3_card2.pack()

    p3_body.add(player3_card1)
    p3_body.add(player3_card2)

    # PLAYER 4
    p4_body = tk.PanedWindow(middle_frame, orient=HORIZONTAL, bg=bg_color)

    player4_card1 = tk.Frame(p4_body, bg=bg_color, padx=8)
    p4_c1_img = tk.PhotoImage(file=f"{card_image_path}{player4_card1_img}.png").subsample(7)
    p4_card1 = Canvas(player4_card1, width=width_main_card, height=height_cards, bg=bg_color, bd=0,
                      highlightthickness=0)
    p4_card1.create_image(width_cards / 1.1, width_cards / 1.1, image=p4_c1_img)
    p4_card1.pack()

    player4_card2 = tk.Frame(p4_body, bg=bg_color)
    p4_c2_img = tk.PhotoImage(file=f"{card_image_path}{player4_card2_img}.png").subsample(7)
    p4_card2 = Canvas(player4_card2, width=width_main_card, height=height_cards, bg=bg_color, bd=0,
                      highlightthickness=0)
    p4_card2.create_image(width_cards / 2, width_cards / 1.1, image=p4_c2_img)
    p4_card2.pack()

    p4_body.add(player4_card1)
    p4_body.add(player4_card2)

    # BOARD
    board = tk.PanedWindow(middle_frame, orient=HORIZONTAL, bg=bg_color)

    board_card1 = tk.Frame(board, bg=bg_color, padx=8)
    b_c1_img = tk.PhotoImage(file=f"{card_image_path}{sub_t1[7]}.png").subsample(9)
    b_card1 = Canvas(board_card1, width=width_cards, height=height_cards, bg=bg_color, bd=0, highlightthickness=0)
    b_card1.create_image(width_cards / 2, width_cards / 1.1, image=b_c1_img)
    b_card1.pack()

    board_card2 = tk.Frame(board, bg=bg_color)
    b_c2_img = tk.PhotoImage(file=f"{card_image_path}{sub_t2[1]}.png").subsample(9)
    b_card2 = Canvas(board_card2, width=width_cards, height=height_cards, bg=bg_color, bd=0, highlightthickness=0)
    b_card2.create_image(width_cards / 2, width_cards / 1.1, image=b_c2_img)
    b_card2.pack()

    board_card3 = tk.Frame(board, bg=bg_color)
    b_c3_img = tk.PhotoImage(file=f"{card_image_path}{sub_t3[1]}.png").subsample(9)
    b_card3 = Canvas(board_card3, width=width_cards, height=height_cards, bg=bg_color, bd=0, highlightthickness=0)
    b_card3.create_image(width_cards / 2, width_cards / 1.1, image=b_c3_img)
    b_card3.pack()

    board.add(board_card1)
    board.add(board_card2)
    board.add(board_card3)

    # Create frames & panedwindow
    left_player.add(p1_title_frame)
    left_player.add(p1_body)
    mid_player.add(p2_title_frame)
    mid_player.add(p2_body)
    right_player.add(p3_title_frame)
    right_player.add(p3_body)

    bottom_player.add(p4_title_frame)
    bottom_player.add(p4_body)
    bottom_player.add(p4_title_frame)
    bottom_player.add(p4_body)
    bottom_board.add(board_title_frame)
    bottom_board.add(board)

    top_frame.add(left_player)
    top_frame.add(mid_player)
    top_frame.add(right_player)

    middle_frame.add(bottom_player)
    middle_frame.add(bottom_board)

    # Continues display
    show_frame(start_frame)
    window.mainloop()
