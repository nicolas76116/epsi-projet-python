import collections
import random


class Card:
    suits = {
        0: "Clubs",
        1: "Diamonds",
        2: "Hearts",
        3: "Spades"
    }

    ranks = {
        0: "Two",
        1: "Three",
        2: "Four",
        3: "Five",
        4: "Six",
        5: "Seven",
        6: "Eight",
        7: "Nine",
        8: "Ten",
        9: "Jack",
        10: "Queen",
        11: "King",
        12: "Ace"
    }

    def __init__(self, rank: int, suit: int):
        self.__rank = rank
        self.__suit = suit

    @property
    def rank(self):
        return self.__rank

    @property
    def suit(self):
        return self.__suit

    def __gt__(self, other):
        if self.rank == other.rank:
            return self.suit > other.suit
        return self.rank > other.rank

    def __lt__(self, other):
        if self.rank == other.rank:
            return self.suit < other.suit
        return self.rank < other.rank

    def __eq__(self, other):
        return (self.rank == other.rank) and (self.suit == other.suit) if isinstance(other, Card) else 0

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return f"{self.ranks[self.rank]} of {self.suits[self.suit]}"

    def __hash__(self):
        return hash(self.__repr__())


class Deck(list):
    """
    list of Cards
    """

    def __init__(self, *args):
        """
        Create a deck of cards
        """
        super().__init__(args)
        self.__seed = None

    @property
    def seed(self):
        return self.__seed

    @seed.setter
    def seed(self, value):
        self.__seed = value

    def shuffle(self, seed=None):
        """
        Shuffle the deck of cards
        :param seed: optional parameter, set a seed for the random shuffle
        """
        if seed is not None:
            self.seed = seed
        else:
            self.seed = random.randint(1, 1000)
        random.seed(seed)
        random.shuffle(self)
        print("-- Deck shuffled --")

    def deal(self):
        """
        Pop the first card of the deck and return it
        :return: the first card of the deck
        """
        res = self.pop(0)
        return res

    def distribute(self, nb_cards: int, nb_players: int) -> list:
        """
        Distribute a number of cards to a number of players
        Each player has a card each in turn
        :param nb_cards: the number of cards to give to each player
        :param nb_players: the number of players
        :return: a list of Decks corresponding to the number of players
        """
        res = []
        for _ in range(nb_players):
            # set of cards to return
            res.append(Deck())

        for _ in range(nb_cards):
            for i in range(nb_players):
                res[i].append(self.deal())
        return res

    def __str__(self):
        return ", ".join([str(card) for card in self])


class PokerDeck(Deck):
    def __init__(self):
        """
        Create a standard deck of 52 cards
        """
        super().__init__()
        for suit in range(len(Card.suits)):
            for rank in range(len(Card.ranks)):
                card = Card(rank, suit)
                self.append(card)
        print("-- New poker deck created --")


class Hand(Deck):
    values = {
        -1: "not a hand",
        0: "High card",
        1: "Pair",
        2: "Two pair",
        3: "Three of a kind",
        4: "Straight",
        5: "Flush",
        6: "Full house",
        7: "Four of a kind",
        8: "Straight flush",
        9: "Royal flush"
    }

    def __init__(self, *args):
        """
        Create a poker hand
        """
        super().__init__()
        self.extend(args[0])
        if not self.is_hand():
            self.__value = -1
        elif self.is_royal_flush():
            self.__value = 9
        elif self.is_straight_flush():
            self.__value = 8
        elif self.is_four_of_a_kind():
            self.__value = 7
        elif self.is_full_house():
            self.__value = 6
        elif self.is_flush():
            self.__value = 5
        elif self.is_straight():
            self.__value = 4
        elif self.is_three_of_a_kind():
            self.__value = 3
        elif self.is_two_pair():
            self.__value = 2
        elif self.is_pair():
            self.__value = 1
        else:
            self.__value = 0

    @property
    def value(self):
        return self.__value

    def __gt__(self, other):
        if self.value == other.value:
            self.sort()
            match self.value:
                case 8:
                    # compare lowest value
                    return self[0] > other[0]
                case 7:
                    # compare the value of the 4 of a kind
                    return self.count_cards()[0][0] > other.count_cards()[0][0]
                case 6:
                    # compare the same 3 values then the 2 same values of each hand
                    if self.count_cards()[0][0] == other.count_cards()[0][0]:
                        return self.count_cards()[1][0] > other.count_cards()[1][0]
                    else:
                        return self.count_cards()[0][0] > other.count_cards()[0][0]
                case 5:
                    # compare sum of the flush
                    return sum(card.rank for card in self) > sum(card.rank for card in other)
                case 4:
                    # compare lowest value
                    return self[0] > other[0]
                case 3:
                    # compare the value of the 3 of a kind
                    return self.count_cards()[0][0] > other.count_cards()[0][0]
                case 2:
                    # compare sum of pairs then fifth card value
                    if (self.count_cards()[0][0] + self.count_cards()[1][0]) == \
                            (other.count_cards()[0][0] + other.count_cards()[1][0]):
                        return self.count_cards()[2] > other.count_cards()[2]
                    else:
                        return (self.count_cards()[0][0] + self.count_cards()[1][0]) < \
                               (other.count_cards()[0][0] + other.count_cards()[1][0])
                case 1:
                    # compare pair value then sum of hand
                    if self.count_cards()[0][0] == other.count_cards()[0][0]:
                        return sum(card.rank for card in self) > sum(card.rank for card in other)
                    else:
                        return self.count_cards()[0][0] > other.count_cards()[0][0]
                case 0:
                    # compare highest value
                    return self[-1] > other[-1]
        return self.value > other.value

    def __lt__(self, other):
        if self.value == other.value:
            self.sort()
            match self.value:
                case 8:
                    # compare lowest value
                    return self[0] < other[0]
                case 7:
                    # compare the value of the 4 of a kind
                    return self.count_cards()[0][0] < other.count_cards()[0][0]
                case 6:
                    # compare the same 3 values then the 2 same values of each hand
                    if self.count_cards()[0][0] == other.count_cards()[0][0]:
                        return self.count_cards()[1][0] < other.count_cards()[1][0]
                    else:
                        return self.count_cards()[0][0] < other.count_cards()[0][0]
                case 5:
                    # compare sum of the flush
                    return sum(card.rank for card in self) < sum(card.rank for card in other)
                case 4:
                    # compare lowest value
                    return self[0] < other[0]
                case 3:
                    # compare the value of the 3 of a kind
                    return self.count_cards()[0][0] < other.count_cards()[0][0]
                case 2:
                    # compare sum of pairs then fifth card value
                    if (self.count_cards()[0][0] + self.count_cards()[1][0]) == \
                            (other.count_cards()[0][0] + other.count_cards()[1][0]):
                        return self.count_cards()[2] < other.count_cards()[2]
                    else:
                        return (self.count_cards()[0][0] + self.count_cards()[1][0]) < \
                               (other.count_cards()[0][0] + other.count_cards()[1][0])
                case 1:
                    # compare pair value then sum of hand
                    if self.count_cards()[0][0] == other.count_cards()[0][0]:
                        return sum(card.rank for card in self) < sum(card.rank for card in other)
                    else:
                        return self.count_cards()[0][0] < other.count_cards()[0][0]
                case 0:
                    # compare highest value
                    return self[-1] < other[-1]
        else:
            return self.value < other.value

    def is_hand(self) -> bool:
        """
        Check if deck has 5 different cards
        :param self: a deck
        :return: True if deck has 5 different cards
        """
        return len(self) == len(set(self)) == 5

    def is_flush(self) -> bool:
        """
        Check if all cards of the hand have the same suit
        :param self: a deck of 5 cards
        :return: True if hand is a flush
        """
        the_suit = self[0].suit
        return self.is_hand() and all([card.suit == the_suit for card in self])

    def is_straight(self, royal=False) -> bool:
        """
        Check if the hand cards ranks are consecutive
        :royal: True if check highest straight
        :return: True if hand is a straight
        """
        self.sort()
        start = 8 if royal else self[0].rank
        end = self[-1].rank
        list_ranks = [card.rank for card in self]
        return self.is_hand() and list_ranks == list(range(start, end + 1))

    def is_straight_flush(self) -> bool:
        """
        Check if the hand cards ranks are consecutive and have the same suit
        :return: True if the hand is a straight flush
        """
        return self.is_straight() and self.is_flush()

    def is_royal_flush(self) -> bool:
        """
        Check if the hand cards have the highest consecutive ranks and have the same suit
        :return: True if the hand is a royal flush
        """
        return self.is_straight(True) and self.is_flush()

    def count_cards(self) -> list:
        """
        Count the occurrence of each card and rank them from the most common one to the least common one
        :return: list of tuples (rank, rank count) in count descending order
        """
        return collections.Counter([card.rank for card in self]).most_common()

    def is_four_of_a_kind(self) -> bool:
        """
        Check if the hand has four cards with the same rank
        :return: True if the hand is a four of a kind
        """
        return self.is_hand() and self.count_cards()[0][1] == 4

    def is_full_house(self) -> bool:
        """
        Check if the hand has three cards with a same rank and two cards with another same rank
        :return: True if the hand is a full house
        """
        return self.is_hand() and self.count_cards()[0][1] == 3 and self.count_cards()[1][1] == 2

    def is_three_of_a_kind(self) -> bool:
        """
        Check if the hand has three cards with the same rank but is not a full house
        :return: True if the hand is a three of a kind
        """
        return self.is_hand() and self.count_cards()[0][1] == 3 and not self.is_full_house()

    def is_two_pair(self) -> bool:
        """
        Check if the hand has two cards with the same rank twice
        :return: True if the hand is a two pair
        """
        return self.is_hand() and self.count_cards()[0][1] == 2 and self.count_cards()[1][1] == 2

    def is_pair(self) -> bool:
        """
        Check if the hand has only two cards with the same rank
        :return: True if the hand is a pair
        """
        return self.is_hand() and self.count_cards()[0][1] == 2 and not self.is_two_pair()

    def is_high_card(self) -> bool:
        """
        Check if the hand is none of the other hands
        :return: True if the hand is a high card
        """
        return self.is_hand() and self.count_cards()[0][1] == 1

    def __str__(self):
        """
        Get the hand value
        :return: hand value
        """
        self.sort()
        match self.value:
            case 7:
                content = f"4 x {Card.ranks[self.count_cards()[0][0]]}"
            case 6:
                content = f"3 x {Card.ranks[self.count_cards()[0][0]]}, " \
                          f"2 x {Card.ranks[self.count_cards()[1][0]]}"
            case 3:
                content = f"3 x {Card.ranks[self.count_cards()[0][0]]}"
            case 2:
                content = f"2 x {Card.ranks[self.count_cards()[0][0]]}, " \
                          f"2 x {Card.ranks[self.count_cards()[1][0]]}"
            case 1:
                content = f"2 x {Card.ranks[self.count_cards()[0][0]]}"
            case 0:
                content = f"{self[-1]}"
            case _:
                content = super(self).__str__()
        return f"{self.values[self.value]}: {content}"
