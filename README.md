# EPSI-Projet-Python

Projet python sur la création d'un jeu de cartes (poker)

## Le but du jeu

C'est un jeu de poker se basant sur les règles du Texas Holdem !
Le joueur joue contre 3 IAs. Les cartes sont distribuées ainsi :  2 cartes par joueur et 3 cartes sur la table de jeu.
Les joueurs décident de s'ils misent ou s'il se couchent pour ainsi vérifier les combinaisons.

Si un joueur a une combinaison gagnante, alors il gagne une somme.

## Avant de commencer

Il est important de consulter le fichier __requirements.txt__

Pour lancer l'application, il est obligatoire d'avoir __Python 3.10__

Pour lancer le jeu, il faut exécuter le code en pointant sur le fichier `main.py`

## Image du jeu

![alt text](assets/images/presentation/interface_presentation.jpg)

Interface du jeu

![alt text](assets/images/presentation/shell_presentation.jpg)

Représentation du jeu sur l'interface


## Ce qu'il reste à faire

- Ajout du paramétrage d'un seed via l'interface de jeu.
- Ajout de l'affichage des combinaisons sur l'interface.
- Ajout d'un système monétaire sur le jeu.


## Crédits

Projet réalisé par des apprenants en Bachelor 3 à l'EPSI dans le cadre d'un projet python sur un temps limité.

